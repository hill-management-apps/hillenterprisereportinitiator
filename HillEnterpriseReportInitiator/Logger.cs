﻿using System;
using System.Diagnostics.Tracing;
using System.IO;

namespace HillEnterpriseReportInitiator
{
    public class Logger
    {
        private const string LOG_FILE_NAME = @"C:\HillMgt Automation\HillEnterpriseReporting\Initiator.log";

        public Logger()
        {
        }

        public void LogMessage(string msg)
        {
            try
            {
                File.AppendAllText(LOG_FILE_NAME, string.Format("{0}\t{1}\n", DateTime.Now.ToLongTimeString(), msg));
                Console.WriteLine(string.Format("{0}\t{1}", DateTime.Now.ToLongTimeString(), msg));
            }
            catch
            {
                throw;
            }
        }
    }
}
