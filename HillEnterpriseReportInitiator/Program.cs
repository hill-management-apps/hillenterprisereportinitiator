﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace HillEnterpriseReportInitiator
{
    class Program
    {
        private const string SQL_STMT_UPDATE =
            "insert into HS_PROCESSING (USERID, RPTNAME, CreateTime, CompleteTime, Status, CompletionMessage, ProgramName, IPAddress, Parms, Email) " +
            "VALUES ('{0}', '{1}', '{2}', NULL, 'Ready', NULL, '{3}', NULL, '{4}', NULL ) ";

        const string HS_CONNECTION_STRING = @"data source=HSSQL2012\HSSQLSERVER;initial catalog=HOMESALES;Integrated Security=true;";
        private static Logger logger = null;

        static void Main(string[] args)
        {
            logger = new Logger();
            logger.LogMessage("Program HillEnterpriseReportInitiator Started.");

            // args[0] = Report Name
            // args[1] = Program Name
            // args[2] = Parameters -- If there are more than one parameter, separate them with a pipe "|"

            // OR...

            // If args[0] = "Display" then
            //    args[1] = Report Name
            // In this case, the top 10 rows from HS_PROCESSING having RPTNAME = args[1] will be displayed on the console.

            if (args.Length < 2)
            {
                logger.LogMessage("Invalid argunents.");
                Environment.Exit(-1);
            }

            if (args[0].ToLower() == "display")
                DisplayMode(args);
            else
            {
                string reportName = args[0];
                string progName = args[1];
                string parms = args[2].Replace("|", " ");

                try
                {
                    SqlAgent sa = new SqlAgent(logger);
                    string sqlS = string.Format(SQL_STMT_UPDATE, Environment.UserName, reportName, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), progName, parms);
                    sa.ExecuteNonQuery(sqlS, HS_CONNECTION_STRING);

                    Environment.Exit(0);
                }
                catch (Exception ex)
                {
                    logger.LogMessage(string.Format("Error occured: {0}", ex.Message));
                    Environment.Exit(-1);
                }
            }
        }

        public static void DisplayMode(string[] args)
        {
            string sql = string.Format("select top 10 userid, createTime, CompleteTime, Status, CompletionMessage from hs_processing where rptname = '{0}' ", args[1]);
            string msg = string.Empty;

            SqlAgent sa = new SqlAgent(logger);
            DataSet ds = sa.ExecuteQuery(sql, HS_CONNECTION_STRING);

            msg = "User ID         Create Time             Complete Time           Status  Message";
            logger.LogMessage(msg);

            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                msg = string.Format("{0}\t\t{1}\t{2}\t{3}\t{4}",
                    dr[0].ToString(), dr[1].ToString(), dr[2].ToString(), dr[3].ToString(), dr[4].ToString());
                logger.LogMessage(msg);
            }

            logger.LogMessage(" ");
            logger.LogMessage("Press any key to continue...");
            Console.ReadLine();
        }
    }
}
